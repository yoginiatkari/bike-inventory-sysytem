-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2021 at 01:17 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bike_inventary_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `bike_details`
--

CREATE TABLE `bike_details` (
  `id` int(50) NOT NULL,
  `b_company` varchar(255) NOT NULL,
  `b_model` varchar(255) NOT NULL,
  `price` bigint(20) NOT NULL,
  `colour` text NOT NULL,
  `man_year` year(4) NOT NULL,
  `cr_date` date NOT NULL,
  `up_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bike_details`
--

INSERT INTO `bike_details` (`id`, `b_company`, `b_model`, `price`, `colour`, `man_year`, `cr_date`, `up_date`) VALUES
(1, 'honda', 'activa 3g', 62000, 'brown', 2015, '0000-00-00', '0000-00-00'),
(2, 'honda', 'activa 4g', 72000, 'black', 2016, '0000-00-00', '0000-00-00'),
(3, 'honda', 'activa 5g', 82000, 'red', 2017, '0000-00-00', '0000-00-00'),
(4, 'honda', 'cb shine', 72000, 'gray', 2016, '0000-00-00', '0000-00-00'),
(5, 'honda', 'sp shine', 86000, 'gold', 2019, '0000-00-00', '0000-00-00'),
(6, 'honda', 'hornet', 120000, 'orange', 2019, '0000-00-00', '0000-00-00'),
(7, 'bajaj', 'pulsar 150', 105000, 'red', 2015, '0000-00-00', '0000-00-00'),
(8, 'bajaj', 'pulsar 220', 120000, 'blue', 2018, '0000-00-00', '0000-00-00'),
(9, 'ktm', 'rc 200', 130000, 'orange', 2019, '0000-00-00', '0000-00-00'),
(10, 'ktm', 'rc 390', 150000, 'black', 2020, '0000-00-00', '0000-00-00'),
(11, 'royal enfield', 'classic 350', 200000, 'gray', 2012, '0000-00-00', '0000-00-00'),
(12, 'royal enfield', 'classic', 180000, 'black', 2009, '0000-00-00', '0000-00-00'),
(13, 'yamaha', 'r15', 170000, 'blue', 2015, '0000-00-00', '0000-00-00'),
(14, 'suzuki', 'hayabusa', 2500000, 'blue', 2018, '0000-00-00', '0000-00-00'),
(15, 'kawasaki', 'ninja', 3000000, 'black', 2020, '0000-00-00', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bike_details`
--
ALTER TABLE `bike_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bike_details`
--
ALTER TABLE `bike_details`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
